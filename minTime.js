"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'minTime' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY batchSize; how many task can perform from numtask[i]
 *  2. INTEGER_ARRAY processingTime
 *  3. INTEGER_ARRAY numTasks
 */

function GetMod(num, mod) {
  return Math.ceil(num / mod) * mod;
}

function minTime(batchSize, processingTime, numTasks) {
  // Write your code here
  let batchLen = batchSize.length;
  let minTime = 0;

  for (let i = 0; i < batchLen; i++) {
    let res =
      (GetMod(numTasks[i], batchSize[i]) * processingTime[i]) / batchSize[i];
    if (res > minTime) minTime = res;
  }
  return minTime;
}

function main() {
  //const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const batchSizeCount = parseInt(readLine().trim(), 10);

  let batchSize = [];

  for (let i = 0; i < batchSizeCount; i++) {
    const batchSizeItem = parseInt(readLine().trim(), 10);
    batchSize.push(batchSizeItem);
  }

  const processingTimeCount = parseInt(readLine().trim(), 10);

  let processingTime = [];

  for (let i = 0; i < processingTimeCount; i++) {
    const processingTimeItem = parseInt(readLine().trim(), 10);
    processingTime.push(processingTimeItem);
  }

  const numTasksCount = parseInt(readLine().trim(), 10);

  let numTasks = [];

  for (let i = 0; i < numTasksCount; i++) {
    const numTasksItem = parseInt(readLine().trim(), 10);
    numTasks.push(numTasksItem);
  }

  const result = minTime(batchSize, processingTime, numTasks);
  console.log(result);

  //ws.write(result + "\n");

  //ws.end();
}
