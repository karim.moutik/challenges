var assert = require("assert");

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function (nums1, nums2) {
  let merged = [];
  let res = 0;
  let i = 0,
    j = 0,
    k = 0;
  while (i < nums1.length && j < nums2.length) {
    if (nums1[i] < nums2[j]) merged[k++] = nums1[i++];
    else merged[k++] = nums2[j++];
  }
  while (i < nums1.length) merged[k++] = nums1[i++];
  while (j < nums2.length) merged[k++] = nums2[j++];

  if (merged.length === 0) return 0;

  if (merged.length % 2 === 0) {
    res = (merged[merged.length / 2 - 1] + merged[merged.length / 2]) / 2;
  } else {
    res = merged[(merged.length - 1) / 2];
  }
  return res;
};

assert(findMedianSortedArrays([], [2, 5, 6]) === 5);
assert(findMedianSortedArrays([], [2, 5, 6]) === 5);
