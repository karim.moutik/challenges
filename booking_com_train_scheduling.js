/*
    6
    msg 3 : ((750)1000)
    msg 4 : (1000)
    msg 1 : (((250)250)1000)
    msg 2 : ((1000)1000)
    msg 5 : ()
    msg 6 : ()

    https://www.hackerrank.com/contests/booking-com-passions-hacked-frontend/challenges/javascript-compiler/problem
*/

function isWhiteSpace(ch) {
  if (ch == " " || ch == "\t" || ch == "\n") return true;
  return false;
}

function parseParanthesis(line) {
  let regx = /[(+)+]/gi;
  line = line.replace(regx, " ");
  let res = line.split(" ").reduce((a, b) => +a + +b);
  return res;
}

function processData(input) {
  let lines = input.split("\n");
  let messages = [];
  let sorted = [];
  for (let i = 1; i < lines.length; i++) {
    let parsedline = lines[i].split(":");
    let pos = parseParanthesis(parsedline[1].trim());
    if (pos === 0) sorted.push(parsedline[0].trim());
    else
      messages.push({
        msg: parsedline[0].trim(),
        pos: pos,
      });
  }
  sorted.forEach((line) => console.log(`${line}.`));
  messages.sort((x, y) => x.pos - y.pos);
  messages.forEach((line) =>
    line.pos > 0 ? console.log(`${line.msg}.`) : null
  );
}

let lines = `6
msg 3 : ((750)1000)
msg 4 : (1000)
msg 1 : (((250)250)1000)
msg 2 : ((1000)1000)
msg 5 : ()
msg 6 : ()`;

processData(lines);
