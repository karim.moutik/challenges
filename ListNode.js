function ListNode(val, next) {
  this.val = val === undefined ? 0 : val;
  this.next = next === undefined ? null : next;
}

function insertToList(arr) {
  let l1 = null;
  for (let i = arr.length - 1; i >= 0; i--) {
    l1 = new ListNode(arr[i], l1);
  }
  return l1;
}

module.exports = {
  ListNode,
  insertToList,
};
