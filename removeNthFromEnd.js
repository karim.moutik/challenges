const { insertToList } = require("./ListNode.js");

var removeNthFromEnd = function (head, n) {
  head = removeit(head, n);
  return head;
};

var removeit = function (node, n) {
  let index = {};
  let head = node;
  let i = 1;
  if (node === null) return null;
  while (node !== null) {
    index[i] = node;
    node = node.next;
    i++;
  }
  let pos = i - n - 1;
  if (pos == 0) return head.next;
  if (pos >= 1) {
    if (index[pos]) {
      let prevNode = index[pos];
      prevNode.next = index[i - n + 1] || null;
    }
  }
  return head;
};

let l1 = insertToList([1, 2, 3]);
let l2 = insertToList([9, 9, 9, 9]);

console.log(JSON.stringify(removeNthFromEnd(l1, 1)));
