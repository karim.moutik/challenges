"use strict";

const fs = require("fs");
const http = require("http");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'getArticleTitles' function below.
 *
 * The function is expected to return a STRING_ARRAY.
 * The function accepts STRING author as parameter.
 *
 * URL for cut and paste:
 * https://jsonmock.hackerrank.com/api/articles?author=<authorName>&page=<num>
 *
 */
const URL = "http://jsonmock.hackerrank.com/api/articles";

const setTitles = (articles) => {
  let titles = [];
  articles.forEach((article) => {
    // Check if the title exists
    if (article.title) {
      titles.push(article.title);
    } else if (article.story_title) {
      // Check if the title is null && sotry_title is set
      titles.push(article.story_title);
    }
  });
  return titles;
};

async function getArticles(author, page) {
  return new Promise(function (res, rej) {
    http.get(`${URL}?author=${author}&page=${page}`, (response) => {
      response.setEncoding("utf8");
      let rawData = "";
      response.on("data", (chunk) => {
        rawData += chunk;
      });
      response
        .on("end", () => {
          res(JSON.parse(rawData));
        })
        .on("error", (e) => {
          rej(e);
        });
    });
  });
}

async function getArticleTitles(author) {
  let titles = [];
  let total_pages = 1;
  let page = 1;
  let allPages = [];
  if (author === null) return titles;
  try {
    let res = await getArticles(author, page);
    total_pages = res.total_pages;
    page = parseInt(res.page);
    console.log("page ", total_pages);
    titles = setTitles(res.data);
    for (let current_page = page + 1; current_page <= 10; current_page++) {
      allPages.push(getArticles(author, current_page));
    }
    let response = await Promise.all(allPages);
    response.forEach((article) => {
      console.log(article);
      titles.push(...setTitles(article.data));
    });
    return titles;
  } catch (e) {
    return { error: e };
  }
}

getArticleTitles("coloneltcb").then((res) => console.log(res));
